package com.test.logapp.repository;

import com.test.logapp.entity.Event;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class EventRepositoryTest {

    @Autowired
    EventRepository eventRepository;

    Event event = new Event("xyz", 10, "12", true);

    @Test
    void saveEvent() {
        eventRepository.save(event);
        List<Event> eventList = eventRepository.findAll();
        assertThat(eventList).extracting(Event::getId).containsOnly("xyz");
        assertThat(eventList).extracting(Event::getHost).containsOnly("12");
        assertThat(eventList).extracting(Event::isAlert).containsOnly(true);
        assertThat(eventList).extracting(Event::getDuration).containsOnly(Long.valueOf(10));

    }
}