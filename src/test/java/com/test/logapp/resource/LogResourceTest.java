package com.test.logapp.resource;

import com.test.logapp.entity.Event;
import com.test.logapp.service.LogReader;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(LogResource.class)
class LogResourceTest {

    @MockBean
    LogReader logReader;

    @Autowired
    MockMvc mockMvc;

    @Test
    void getLogs() throws Exception {
        Event event = new Event("xyz", 10, "12", true);
        when(logReader.readFileAndPersist()).thenReturn(Collections.singletonList(event));

        mockMvc.perform(get("/logs"))
                .andExpect(status().isOk());
    }
}