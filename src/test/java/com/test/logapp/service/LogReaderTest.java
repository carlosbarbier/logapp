package com.test.logapp.service;

import com.test.logapp.entity.Event;
import com.test.logapp.repository.EventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LogReaderTest {
    @Mock
    EventRepository eventRepository;

    Event event = new Event("xyz", 10, "12", true);

    private String fileName = "src/test/resources/logfile.txt";

    @InjectMocks
    LogReader logReader;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(logReader, "fileName", fileName);
    }

    @Test
    void testReadFileSuccess() throws IOException {
        when(eventRepository.save(any(Event.class))).thenReturn(event);
        logReader.readFileAndPersist();
        verify(eventRepository, times(1)).save(any());
    }

    @Test
    void testReadFileException() {
        when(eventRepository.save(any(Event.class))).thenThrow(new RuntimeException(""));
        assertThrows(RuntimeException.class, () -> logReader.readFileAndPersist());
    }


}