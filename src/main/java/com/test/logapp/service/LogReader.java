package com.test.logapp.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.logapp.dto.EventDto;
import com.test.logapp.entity.Event;
import com.test.logapp.exception.LogProcessingException;
import com.test.logapp.repository.EventRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Slf4j
@Service
public class LogReader {

    @Autowired
    private EventRepository repository;

    private ObjectMapper objectMapper;

    private final Map<String, Long> map = new HashMap<>();

    private final List<Event>eventDtoList= new ArrayList<>();

    private List<String> fileList;

    private String fileName="src/main/resources/static/logfile.txt";

    public List<Event> readFileAndPersist() throws IOException {
        fileList = getFileList(fileName);
        if (fileList.size()>0){
            objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            fileList.forEach(line->{
                EventDto eventDto;
                try {
                    eventDto = objectMapper.readValue(line, EventDto.class);
                    boolean isIdPresent = map.containsKey(eventDto.getId());
                    if (isIdPresent) {
                        Long startTimestamp = map.get(eventDto.getId());
                        long timeTaken = (eventDto.getTimestamp() - startTimestamp);
                        Event event = new Event();
                        event.setDuration(timeTaken);
                        event.setHost(eventDto.getHost());
                        event.setId(eventDto.getId());
                        if (timeTaken > 4) {
                            event.setAlert(true);
                            Event  eventSaved = repository.save(event);
                            eventDtoList.add(eventSaved);
                            log.info("event save in db {}",  eventSaved);
                        }

                    } else {
                        map.put(eventDto.getId(), eventDto.getTimestamp());
                    }

                } catch (Exception ex) {
                    log.error("Exception occurred while processing the file");
                   throw new LogProcessingException("Exception occurred while processing the file");
                }
            });
        }

        return eventDtoList;

    }

    /**
     *
     * @param file path
     * @return list of each line of the file
     * @throws IOException
     */
    private List<String> getFileList(String file) throws IOException {
        List<String>fileList= new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            stream.forEach(fileList::add);
        }
        return fileList;
    }
}
