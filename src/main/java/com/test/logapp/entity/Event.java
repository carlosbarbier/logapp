package com.test.logapp.entity;

import com.test.logapp.dto.EventDto;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Event {
    @Id
    private String id;
    private long duration;
    private String host;
    private boolean alert;
}


