package com.test.logapp.exception;

public class LogProcessingException extends RuntimeException {
    public LogProcessingException(String errorMessage) {
        super(errorMessage);
    }
}
