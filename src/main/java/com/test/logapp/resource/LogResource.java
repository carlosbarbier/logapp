package com.test.logapp.resource;

import com.test.logapp.dto.EventDto;
import com.test.logapp.entity.Event;
import com.test.logapp.service.LogReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
public class LogResource {
    @Autowired
    private LogReader logReader;

    @GetMapping("/logs")
    public List<Event> getLogs() throws IOException {
        log.info("getting application logs");
        return logReader.readFileAndPersist();
    }
}
