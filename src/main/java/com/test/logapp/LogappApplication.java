package com.test.logapp;

import com.test.logapp.service.LogReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogappApplication {
    public static void main(String[] args) {
        SpringApplication.run(LogappApplication.class, args);
    }
}
