package com.test.logapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventDto {
    private String id;
    private String host;
    private boolean alert;
    private long timestamp;
}
